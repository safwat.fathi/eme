# **Webpack** config guide

## Webpack version:

webpack 5

**_note:_** when runing webpack in production mode in this version it will declare this message => "[DEP_WEBPACK_COMPILATION_ASSETS] DeprecationWarning: Compilation.assets will be frozen in future, all modifications are deprecated.
BREAKING CHANGE: No more changes should happen to Compilation. assets after sealing the Compilation. Do changes to assets earlier,
e. g. in Compilation.hooks.processAssets. Make sure to select an appropriate stage from Compilation.PROCESS ASSETS_STAGE".

<!-- write here what this message means and -->

---

## Project stack (static website):

- HTML
- SASS
- Vanilla JS

### Libraries used:

- Bootstrap
- Slick carousel
- Google Maps

---

## Webpack is doing the following:

- Bundling all JS into one file and transpiling it to compaitable version to support browsers that do not support ES5+
- Compiling SASS into CSS.
- Bundling all CSS used int the project into one CSS file and minify it
- Managing assets (images, fonts).
- Optimizing images.

---

## How ?

### Bundling JavaScript

### Compiling SASS

### Bundling CSS

### Managing assets

const path = require("path");
const webpack = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

// check for build or development mode
const isDevelopment = process.env.NODE_ENV === "development";

module.exports = {
  // entry point
  entry: "./src/js/index.js",
  // output path
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].bundle.js",
    publicPath: ".",
  },
  // optimize assets
  optimization: {
    minimize: !isDevelopment,
    minimizer: [
      new CssMinimizerPlugin({
        sourceMap: true,
      }),
    ],
  },
  // plugins
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      Popper: ["popper.js", "default"],
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].bundle.css",
    }),
  ],
  // module
  module: {
    rules: [
      // -------
      // HTML
      // -------
      {
        test: /\.html$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
            },
          },
          "extract-loader",
          {
            loader: "html-loader",
            options: {
              minimize: false,
            },
          },
        ],
      },
      // -----------
      // javaScript
      // -----------
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      // -------
      // sass
      // -------
      {
        test: /\.s[ac]ss$/i,
        exclude: /(node_modules)/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              // modules: true,
              url: true,
              sourceMap: true,
            },
          },
          // {
          //   loader: "postcss-loader",
          // },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass"),
              // modules: true,
              sourceMap: true,
            },
          },
        ],
      },
      // -----------------
      // importing images
      // -----------------
      {
        test: /\.(png|jpe?g|gif|svg|)$/i,
        exclude: /font/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
              name: "[name].[ext]",
              outputPath: "/img/",
            },
          },
        ],
      },
      // ----------------
      // importing fonts
      // ----------------
      {
        test: /\.(svg|eot|ttf|woff|woff2)$/i,
        exclude: /img/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
              name: "[name].[ext]",
              outputPath: "/font/",
            },
          },
        ],
      },
    ],
  },
  // when importing files in our entry point it auto adds the file extension
  resolve: {
    extensions: [
      ".js",
      ".scss",
      ".gif",
      ".png",
      ".jpg",
      ".jpeg",
      ".svg",
      ".html",
    ],
  },
  mode: "development",
};

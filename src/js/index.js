// bootstrap package
import "bootstrap";
// slick carousel package
import "slick-carousel";
// google maps API
import loadGoogleMapsApi from "load-google-maps-api";
// sass
import "../sass/main";
// html
import "../about-us";
import "../contact-us";
import "../services";
import "../consortium";
import "../index";

// sliders
// --------
window.addEventListener("DOMContentLoaded", (e) => {
  console.log("document & css loaded, images yet to be loaded");
  try {
    // main slider
    const $homeLink = document.querySelector("a[href='#mast_head']");

    $homeLink.addEventListener("click", (e) => {
      $(".mast_head_slider").slick("slickGoTo", 0);
    });

    $(".mast_head_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      infinite: false,
      centerMode: false,
      focusOnSelect: false,
      arrows: false,
      dots: true,
      lazyLoad: "ondemand",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });
  } catch (err) {
    console.log(err);
  }
});

(() => {
  // slider for sub pages
  // --------------------
  try {
    // main controls
    const $prevBtn = document.querySelector(".controls .prev");
    const $nextBtn = document.querySelector(".controls .next");

    $(".slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      centerMode: false,
      focusOnSelect: false,
      prevArrow: $prevBtn,
      nextArrow: $nextBtn,
    });

    // --------------
    // sub sliders
    // --------------
    $(".sub_slider.itida_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      centerMode: false,
      variableWidth: false,
      focusOnSelect: false,
      draggable: false,
      prevArrow: `<button class="sub_prev sub_prev--itida"></button>`,
      nextArrow: `<button class="sub_next sub_next--itida"></button>`,
    });
    // ===========================
    $(".sub_slider.ideaspace_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      centerMode: false,
      variableWidth: false,
      focusOnSelect: false,
      draggable: false,
      prevArrow: `<button class="sub_prev sub_prev--idea"></button>`,
      nextArrow: `<button class="sub_next sub_next--idea"></button>`,
    });
    // ===========================
    $(".sub_slider.sn3a_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      centerMode: false,
      variableWidth: false,
      focusOnSelect: false,
      draggable: false,
      prevArrow: `<button class="sub_prev sub_prev--sn3a"></button>`,
      nextArrow: `<button class="sub_next sub_next--sn3a"></button>`,
    });
    // ===========================
    $(".sub_slider.etsal_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      infinite: false,
      centerMode: false,
      variableWidth: false,
      focusOnSelect: false,
      draggable: false,
      prevArrow: `<button class="sub_prev sub_prev--etsal"></button>`,
      nextArrow: `<button class="sub_next sub_next--etsal"></button>`,
    });
  } catch (err) {
    console.log(err);
  }
  // ===========================
  // ===========================

  try {
    // borg: 30.8557836,29.5692928
    // assuit: 27.2722593,31.2612869

    // addresses
    const $borgAddress = document.querySelector(".address .address_borg");
    const $assuitAddress = document.querySelector(".address .address_assuit");
    // locations
    const borgLoc = { lat: 30.8557836, lng: 29.5692928 };
    const assuitLoc = { lat: 27.2722593, lng: 31.2612869 };

    loadGoogleMapsApi().then((googleMaps) => {
      const map = new googleMaps.Map(document.getElementById("map"), {
        zoom: 10,
        center: borgLoc,
      });

      // assuit location
      const assuitLocMarker = new googleMaps.Marker({
        position: assuitLoc,
        map,
        title: "Assuit EME Hub Location",
      });
      // borg location
      const borgLocMarker = new googleMaps.Marker({
        position: borgLoc,
        map,
        title: "Borg El Arab EME Hub Location",
      });

      borgLocMarker.setMap(map);

      // newLocation function
      function newLocation(newLat, newLng, oldMarker, newMarker) {
        map.setCenter({
          lat: newLat,
          lng: newLng,
        });

        oldMarker.setMap(null);
        newMarker.setMap(map);
      }

      // on address click change center & marker
      [$borgAddress, $assuitAddress].forEach((address, i) => {
        address.addEventListener("click", (e) => {
          let target = e.target;

          // adding class active to address
          // @ts-ignore
          if (target.classList.contains("active")) {
            console.log("target is active");
          } else {
            console.log();
            $borgAddress.classList.remove("active");
            $assuitAddress.classList.remove("active");
            // @ts-ignore
            target.classList.add("active");
          }

          // changing map marker based on address
          // @ts-ignore
          if (target.dataset.name === "borg") {
            newLocation(
              borgLoc.lat,
              borgLoc.lng,
              assuitLocMarker,
              borgLocMarker
            );
          } else {
            // prevEl.classList.toggle("active");
            newLocation(
              assuitLoc.lat,
              assuitLoc.lng,
              borgLocMarker,
              assuitLocMarker
            );
          }
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
})();
